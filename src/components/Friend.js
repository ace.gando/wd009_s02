import React from 'react';

class Friend extends React.Component{
	
	render(){
		return(
			<div class="bg-warning mt-3">
				<ul>
					{
						this.props.friends.map(friend => {
							return(
								<li>
									My Friend {friend.name} is {friend.age} years old 
									<button onClick={ () => this.props.deleteFriend(friend.id) }> &times; </button>
								</li>
							)
						})
					}
				</ul>
			</div>
		)
	}
}

export default Friend;