import React from 'react';

class EditFriend extends React.Component {

	state = {
		name: null,
		age: null,
		id: null
	}

	handleChangeSelect = (e) => {
		this.setState(
			this.props.friends.find( friend =>{
				return friend.id === e.target.value
			})
		)
	}

	handleChange = (e) => {
		this.setState({[e.target.id] : e.target.value})
	}

	componentDidMount = () => {
		this.setState(this.props.friends[0])
	}

	handleSubmit = (e) => {
		e.preventDefault()
		this.props.editFriend(this.state.id, this.state)
	}

	render(){
		return(
			<div id='edit-friend'>
				<form action="" onSubmit={this.handleSubmit}>
					<label htmlFor="edit-friend">Edit Friend:</label>
					<select 
					name="edit-friend" 
					id="edit-friend"
					className="form-control"
					onChange={this.handleChangeSelect}
					>
						{
							this.props.friends.map(friend => {
								return(
									<option value={friend.id}>{friend.name}</option>
								)
							})
						}
					</select>
					
					Edit Friend Name
					<input
						type="text"
						name="name"
						id="name"
						className="form-control"
						value={this.state.name}
						onChange={this.handleChange}
					/>
					<input
						type="number"
						name="age"
						id="age"
						className="form-control"
						value={this.state.age}
						onChange={this.handleChange}
					/>
					
					<button className="btn btn-primary">Edit</button>
					
				</form>
			</div>
		)
	}
}

export default EditFriend;