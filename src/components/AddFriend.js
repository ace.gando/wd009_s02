import React from 'react';

class AddFriend extends React.Component{

	state = {
		name: null,
		age: null
	}

	handleChange = (e) => {
		console.log(e.target.id)
		this.setState({
			[e.target.id] : e.target.value
		})
	}

	handleSubmit = (e) => {
		e.preventDefault()

		this.props.addFriend(this.state)

		this.setState({name:null, age:null})
		e.target.reset();
	}

	render(){


		return(
			<div class="bg-dark text-white">
				<h1>Form to add Friend</h1>
				<form onSubmit={this.handleSubmit}>
					<label htmlFor="name">Name:</label>
					<input onChange={ this.handleChange } id="name" type="text"/>
					
					<label htmlFor="age">Age:</label>
					<input onChange={ this.handleChange } id="age" type="number"/>

					<button>Add Ninja</button>
				</form>
			</div>
		)
	}
}

export default AddFriend;