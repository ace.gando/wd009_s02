import React from 'react';
import AddFriend from './components/AddFriend';
import EditFriend from './components/EditFriend';
import Friend from './components/Friend';
import uuid from 'uuid/v1';

class App extends React.Component{

  state = {
    friends : [
      {
        name: "Kenji",
        age: 33,
        id: uuid()
      },
      {
        name: "Ottomo",
        age: 46,
        id: uuid()
      }
    ]
  }

  handleAddFriend = (friend) => {
    friend.id = uuid()
    this.setState({
      friends : [
        ...this.state.friends,
        friend
      ]
    })
  }

  handleDeleteFriend = (id) => {
    // filter
    let updatedFriends = this.state.friends.filter( friend => {
      return friend.id !== id
    });

    this.setState({
      friends: updatedFriends
    })
  }

  handleEditFriend = (id, updatedFriend) => {
    // this.setState({
    //   friends : [

    //   ]
    //   friends.map(friend => {
    //     return friend.id == id ? updatedFriend : friend
    //   })
    // })
    let updatedFriends = this.state.friends.map( friend => {
     return friend.id == id ? updatedFriend : friend
    })
    this.setState({friends : updatedFriends})
    console.log(updatedFriends)
  }

  render(){
    return(
      <React.Fragment>
        <h1>Hello World</h1>
        <AddFriend addFriend={this.handleAddFriend}/>
        <Friend 
          friends={this.state.friends} 
          deleteFriend={this.handleDeleteFriend}
        />
        <EditFriend
          friends={this.state.friends}
          editFriend={this.handleEditFriend}
        />
      </React.Fragment>
    )
  }
}

export default App;